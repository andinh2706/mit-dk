import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createDrawerNavigator } from '@react-navigation/drawer'
import TodoStack from './routes/todo-stack'
import HelloWorldStack from './routes/hello-world-stack'
import React from 'react'
// import AuthenticationStack from './routes/authentication-world-stack'

const Drawer = createDrawerNavigator()

export default function App() {
    return (
        <NavigationContainer>
            <Drawer.Navigator>
                {/* <Drawer.Screen
                    name='FirstLogin'
                    component={AuthenticationStack}
                /> */}
                <Drawer.Screen name='HelloWorld' component={HelloWorldStack} />
                <Drawer.Screen name='TodoList' component={TodoStack} />
            </Drawer.Navigator>
        </NavigationContainer>
    )
}
