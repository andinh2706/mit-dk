import React from 'react'
import { View, Button } from 'react-native'

export default function HelloWorld(props) {
    const { navigation } = props
    return (
        <View>
            <Button
                title='Go to to do list'
                onPress={() => navigation.navigate('TodoList')}
            />
        </View>
    )
}
