import React, { useContext, useState } from 'react'
import { View, Button, TextInput, StyleSheet } from 'react-native'
import { ToDoContext, addTodo } from 'mit-core'

export default function TodoCreate(props) {
    const [text, setText] = useState('')
    const { todoDispatcher } = useContext(ToDoContext)
    const { navigation } = props
    const addTodoHandler = () => {
        addTodo(todoDispatcher, text, () => navigation.navigate('TodoList'))
    }

    const handleInputChange = (e) => {
        const { text } = e.nativeEvent
        setText(text)
    }

    return (
        <View>
            <Button title='Create to do' onPress={() => addTodoHandler()} />
            <TextInput
                type='text'
                onChange={(e) => handleInputChange(e)}
                style={styles.input}
                placeholder='Please typing here'
            />
        </View>
    )
}

const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        marginTop: 10
    }
})
