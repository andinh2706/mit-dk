import React, { useContext } from 'react'
import { Text, View, Button, FlatList } from 'react-native'
import { ToDoContext } from 'mit-core'

export default function TodoList(props) {
    const { todoState } = useContext(ToDoContext)
    const { navigation } = props
    const convertData = (todos) => {
        return todos.map((todo) => ({ todo: todo, key: todo }))
    }

    const checkTodoList = () => {
        return todoState.todos.length ? (
            <FlatList
                data={convertData(todoState.todos)}
                renderItem={({ item }) => {
                    return (
                        <Text
                            style={{
                                backgroundColor: '#8B15E6',
                                color: 'white',
                                padding: 10,
                                margin: 10
                            }}
                        >
                            {item.todo}
                        </Text>
                    )
                }}
            />
        ) : (
            <Text>There is no task for you now</Text>
        )
    }
    return (
        <View style={{ flex: 1, flexDirection: 'column' }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    maxHeight: 38,
                    margin: 10,
                    justifyContent: 'space-between'
                }}
            >
                <Button
                    title='Go to hello world'
                    onPress={() => navigation.navigate('HelloWorld')}
                />
                <Button
                    title='Create to do'
                    onPress={() => navigation.navigate('TodoCreate')}
                />
            </View>
            {checkTodoList()}
        </View>
    )
}
