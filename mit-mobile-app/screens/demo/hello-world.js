import React from 'react'
import { View, Button } from 'react-native'

export default function HelloWorld(props) {
    const { navigation } = props
    return (
        <View>
            <Button
                title='Go to content'
                onPress={() => navigation.navigate('HelloWorldContent')}
            />
        </View>
    )
}
