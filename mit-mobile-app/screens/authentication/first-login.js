import React, { useContext } from 'react'
import {
    Text,
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ImageBackground,
    Dimensions,
    Modal
} from 'react-native'
import LoginModal from './components/login-modal'
import { ModalContext, openModal } from 'mit-core'

const FirstLogin = () => {
    const { modalState, modalDispatcher } = useContext(ModalContext)
    return (
        <View style={LoginStyles.loginContainer}>
            <Image
                style={LoginStyles.loginLogoImage}
                source={require('../../assets/logo/mit-dk.png')}
            />
            <TouchableOpacity
                onPress={() => openModal(modalDispatcher, <LoginModal />)}
            >
                <View style={LoginStyles.loginButton}>
                    <Text
                        style={[
                            LoginStyles.loginTextColor,
                            LoginStyles.loginTextBold
                        ]}
                    >
                        Login
                    </Text>
                </View>
            </TouchableOpacity>
            <View style={LoginStyles.createProfileTextContainer}>
                <Text style={[LoginStyles.loginTextColor, { marginRight: 5 }]}>
                    Not yet a user?
                </Text>
                <TouchableWithoutFeedback>
                    <Text
                        style={[
                            LoginStyles.loginTextColor,
                            LoginStyles.loginTextBold
                        ]}
                    >
                        Create a profile
                    </Text>
                </TouchableWithoutFeedback>
            </View>
            <ImageBackground
                style={LoginStyles.loginBackground}
                source={require('../../assets/background/login-background.png')}
            >
                <View style={LoginStyles.loginInspectContainer}>
                    <Image
                        style={LoginStyles.loginImageIntersect}
                        source={require('../../assets/background/login-background-intersect.png')}
                    />
                </View>
            </ImageBackground>
            {/* <Modal visible={modalState.show} animationType='slide'>
                {modalState && modalState.content}
            </Modal> */}
        </View>
    )
}
const windowWidth = Dimensions.get('window').width

const LoginStyles = StyleSheet.create({
    loginContainer: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 70
    },
    loginButton: {
        borderWidth: 1,
        borderColor: '#E56A6A',
        borderRadius: 4,
        maxHeight: 40,
        width: 285,
        marginTop: 56,
        marginBottom: 15,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginTextColor: {
        color: '#E56A6A'
    },
    loginTextBold: {
        fontWeight: 'bold'
    },
    loginLogoImage: {
        width: 210,
        height: 62,
        resizeMode: 'contain'
    },
    createProfileTextContainer: {
        flexDirection: 'row'
    },
    loginBackground: {
        width: windowWidth,
        height: windowWidth * 1.4,
        alignItems: 'center',
        marginTop: 50
    },
    loginImageIntersect: {
        width: '100%',
        marginTop: '20%'
    },
    loginInspectContainer: {
        width: '100%',
        height: (windowWidth * 1.4) / 2.2,
        justifyContent: 'center'
    }
})

export default FirstLogin
