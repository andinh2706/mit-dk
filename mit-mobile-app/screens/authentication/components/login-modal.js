import React, { useState } from 'react'
import { View, StyleSheet, useWindowDimensions } from 'react-native'
import { TabView, SceneMap } from 'react-native-tab-view'
import MitForm from './mit-form'
import NemidForm from './nem-id-form'

const LoginModal = () => {
    const [tabIndex, setTabIndex] = useState(0)
    const layout = useWindowDimensions()
    const [tabs] = useState([
        { key: 'mit', title: 'MIT Login' },
        { key: 'nemid', title: 'NEM ID Login' }
    ])
    const renderScene = SceneMap({
        first: MitForm,
        second: NemidForm
    })

    return (
        <View style={loginModalStyles.container}>
            <TabView
                navigationState={{ tabIndex, tabs }}
                renderScene={renderScene}
                onIndexChange={setTabIndex}
                initialLayout={{ width: layout.width }}
            />
        </View>
    )
}

const loginModalStyles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: 'white',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4
    }
})

export default LoginModal
