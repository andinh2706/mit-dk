import React from 'react'
import { Text, View, StyleSheet, Button } from 'react-native'

const MitForm = () => {
    return (
        <View style={loginContainerStyles.container}>
            <View style={loginContainerStyles.formTitle}>
                <Text style={loginContainerStyles.textTitle}>MIT</Text>
                <View style={loginContainerStyles.titleActions}>
                    <Button title='Info' />
                    <Button title='Close' />
                </View>
            </View>
        </View>
    )
}

const loginContainerStyles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        padding: 10
    },
    formTitle: {
        flex: 1,
        width: '100%',
        maxHeight: 30,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    titleActions: {
        flex: 1,
        flexDirection: 'row'
    },
    textTitle: {
        fontSize: 24
    }
})

export default MitForm
