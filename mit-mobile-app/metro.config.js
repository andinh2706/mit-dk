// const path = require('path')

// // react-native >= 0.57
// console.log('Env: ', process.env)
// const extraNodeModules = {
//     'mit-core': path.resolve(`${__dirname}/../mit-core`)
// }
// const watchFolders = [path.resolve(`${__dirname}/../mit-core/`)]

// module.exports = {
//     resolver: {
//         extraNodeModules
//     },
//     watchFolders
// }

console.log('Current environment is:', process.env.NODE_ENV)

const { getDefaultConfig } = require('@expo/metro-config')

const path = require('path')

const defaultConfig = getDefaultConfig(__dirname)

const blacklist = require('metro-config/src/defaults/blacklist')

const mitcoreLib = path.resolve(__dirname, '../mit-core/dist')

let extraNodeModules = {
    'mit-core': mitcoreLib
}

extraNodeModules = new Proxy(extraNodeModules, {
    get: (target, name) =>
        // redirects dependencies referenced from lib/ to local node_modules
        name in target
            ? target[name]
            : path.join(__dirname, `node_modules/${name}`)
})

defaultConfig.watchFolders = [mitcoreLib]

defaultConfig.resolver.blacklistRE = blacklist([/node_modules\/mit-core\/.*/])

defaultConfig.resolver.extraNodeModules = extraNodeModules

module.exports = defaultConfig
