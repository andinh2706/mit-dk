import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import HelloWorld from '../screens/demo/hello-world'
import HelloWorldContent from '../screens/demo/hello-world-content'
const Stack = createStackNavigator()
export default function HelloWorldStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name='HelloWorld' component={HelloWorld} />
            <Stack.Screen
                name='HelloWorldContent'
                component={HelloWorldContent}
            />
        </Stack.Navigator>
    )
}
