import React, { useReducer } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import FirstLogin from '../screens/authentication/first-login'
import { ModalContext, initModal, modalReducer } from 'mit-core'
const Stack = createStackNavigator()
export default function AuthenticationStack() {
    const [modalState, modalDispatcher] = useReducer(modalReducer, initModal)
    return (
        <ModalContext.Provider value={{ modalState, modalDispatcher }}>
            <Stack.Navigator>
                <Stack.Screen
                    name='MIT Login'
                    options={{ headerShown: false }}
                    component={FirstLogin}
                />
            </Stack.Navigator>
        </ModalContext.Provider>
    )
}
