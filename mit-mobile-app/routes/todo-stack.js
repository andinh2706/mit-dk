import { createStackNavigator } from '@react-navigation/stack'
import TodoList from '../screens/demo/todo-list'
import TodoCreate from '../screens/demo/todo-create'
import { ToDoContext, initTodo, todoReducer } from 'mit-core'
import React, { useReducer } from 'react'

const Stack = createStackNavigator()

export default function TodoStack() {
    const [todoState, todoDispatcher] = useReducer(todoReducer, initTodo)
    return (
        <ToDoContext.Provider value={{ todoState, todoDispatcher }}>
            <Stack.Navigator>
                <Stack.Screen name='TodoList' component={TodoList} />
                <Stack.Screen name='TodoCreate' component={TodoCreate} />
            </Stack.Navigator>
        </ToDoContext.Provider>
    )
}
