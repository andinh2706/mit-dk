# mit-core

> MIT core contains all common functionalities which will be used in both MIT webview and MIT mobile application.

[![NPM](https://img.shields.io/npm/v/mit-core.svg)](https://www.npmjs.com/package/mit-core) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save mit-core
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'mit-core'
import 'mit-core/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [MIT](https://github.com/MIT)
