import { LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from '../../const'

export const initAuth = {
    isAuth: false,
    username: null,
    error: false
};


export const authReducer = (state, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {...state, isAuth: true, username: action.payload.username}
        case LOGIN_FAILURE:
            return {...state, error: true}
        case LOGOUT:
            return {...initAuth}
    }
    return state;
}
