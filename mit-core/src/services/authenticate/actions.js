import { LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from '../../const';
import {authUser} from '../../apis'

export const login = async (authDispatcher, {username , password}) => {
    const success = await authUser({username, password})
    if(success)
        authDispatcher({type: LOGIN_SUCCESS, payload: {username}})
    else 
        authDispatcher({type: LOGIN_FAILURE})
        
    return success
}

export const logout = async (authDispatcher) => {
    return modalDispatcher({ type: LOGOUT });
}