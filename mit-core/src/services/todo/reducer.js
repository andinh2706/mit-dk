import { ADD, REMOVE } from '../../const/todo-const'

export const initTodo = {
    todos: []
}

export const todoReducer = (state, action) => {
    switch (action.type) {
        case ADD:
            state = {
                ...state,
                todos: [...state.todos,action.payload.todo]
            };
            break;
        case REMOVE:
            state = {
                ...state,
                todos: state.todos.filter((todo) => todo !== action.payload.todo)
            }
            break
        default:
            break
    }
    return state;
}
