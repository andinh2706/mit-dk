import { ADD, REMOVE } from '../../const/todo-const';
import {postTodo} from '../../apis';


export const addTodo = (todoDispatcher, todo, callback) => {
    console.log("Adding todo")
    postTodo()
    .then(() => {
        console.log("Todo Added");
        todoDispatcher({ type: ADD, payload: { todo: todo } });
        callback && callback();
    });
}

export const removeTodo = (todoDispatcher, todo, callback) => {
    todoDispatcher({ type: REMOVE,  payload: { todo: todo } });
    return callback && callback();
}


