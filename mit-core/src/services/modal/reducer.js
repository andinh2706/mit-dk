import { MODAL_OPEN, MODAL_CLOSE } from '../../const'

export const initModal = {
    content: null,
    show: false
};


export const modalReducer = (state, action) => {
    switch (action.type) {
        case MODAL_OPEN:
            return {...state, show: true, content: action.payload.content}
        case MODAL_CLOSE:
            return {...state, show: false, content: null}
    }
    return state;
}
