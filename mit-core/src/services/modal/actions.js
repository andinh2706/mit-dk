import { MODAL_OPEN, MODAL_CLOSE } from '../../const';


export const openModal = (modalDispatcher, content) => {
    modalDispatcher({ type: MODAL_OPEN, payload: { content } });
}

export const closeModal = (modalDispatcher) => {
    modalDispatcher({ type: MODAL_CLOSE });
}