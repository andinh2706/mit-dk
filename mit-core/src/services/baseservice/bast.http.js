import axios from 'axios'
import config, { masterKey } from './config'
import cookieManager from './cookieManager'

const axiosMasterRequestConfig = {
    baseURL: config.baseApi,
    params: {
        access_token: masterKey
    }
}

export const baseMasterHttp = {
    get: function (url, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosMasterRequestConfig, requestConfig)
            : axiosMasterRequestConfig
        return axios.get(url, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    post: function (url, data, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosMasterRequestConfig, requestConfig)
            : axiosMasterRequestConfig
        return axios.post(url, data, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    puth: function (url, data, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosMasterRequestConfig, requestConfig)
            : axiosMasterRequestConfig
        return axios.puh(url, data, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    delete: function (url, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosMasterRequestConfig, requestConfig)
            : axiosMasterRequestConfig
        return axios.delete(url, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    }
}

const axiosRequestConfig = {
    baseURL: config.baseApi
}

export const baseHttp = {
    get: function (url, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosRequestConfig, requestConfig)
            : axiosRequestConfig
        return axios.get(url, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    post: function (url, data, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosRequestConfig, requestConfig)
            : axiosRequestConfig
        return axios.post(url, data, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    put: function (url, data, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosRequestConfig, requestConfig)
            : axiosRequestConfig
        return axios.put(url, data, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    delete: function (url, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosRequestConfig, requestConfig)
            : axiosRequestConfig
        return axios.delete(url, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    }
}

/**
 * The Authorization will be assigned after login
 */
export const axiosAuthRequestConfig = {
    baseURL: config.baseApi,
    headers: {
        Authorization: 'Bearer ' + cookieManager.getToken(), // This code will get the token from cookie every time reloaded page
        'Content-Type': 'application/json'
    }
}

export const baseAuthHttp = {
    get: function (url, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosAuthRequestConfig, requestConfig)
            : axiosAuthRequestConfig
        return axios.get(url, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    post: function (url, data, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosAuthRequestConfig, requestConfig)
            : axiosAuthRequestConfig
        return axios.post(url, data, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    put: function (url, data, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosAuthRequestConfig, requestConfig)
            : axiosAuthRequestConfig
        return axios.put(url, data, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    },
    delete: function (url, requestConfig) {
        requestConfig = requestConfig
            ? Object.assign({}, axiosAuthRequestConfig, requestConfig)
            : axiosAuthRequestConfig
        return axios.delete(url, {
            ...requestConfig,
            headers: requestConfig.headers
        })
    }
}
