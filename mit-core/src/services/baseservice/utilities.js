export const uniqueId = () => {
    return Math.random().toString(36).substr(2, 16)
}

export const splitFileName = (fileName) => {
    const splitFileName = fileName.split('.')
    return {
        fileName: splitFileName[0],
        extension: splitFileName[1]
    }
}

export const generateNewFileName = (fileName) => {
    const splitedFile = splitFileName(fileName)
    return `${splitedFile.fileName + uniqueId()}.${splitedFile.extension}`
}
