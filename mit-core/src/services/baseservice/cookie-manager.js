const TOKEN = 'TOKEN'
const USER_INFO = 'USER_INFO'

// We can use cookie instead
const cookieManager = {
    getToken: function () {
        return cookieManager.getCookie(TOKEN)
    },
    getUserInfo: function () {
        return cookieManager.getCookie(USER_INFO)
    },
    setToken: function (token, exdays) {
        cookieManager.setCookie(TOKEN, token, exdays)
    },
    setUserInfo: function (userInfo, exdays) {
        cookieManager.setCookie(USER_INFO, userInfo, exdays)
    },
    hasToken: function () {
        return cookieManager.checkCookie(TOKEN)
    },
    removeToken: function () {
        cookieManager.removeCookie(TOKEN)
    },
    removeUserInfo: function () {
        cookieManager.removeCookie(USER_INFO)
    },
    getCookie: function (cname) {
        var name = cname + '='
        var decodedCookie = decodeURIComponent(document.cookie)
        var ca = decodedCookie.split(';')
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i]
            while (c.charAt(0) === ' ') {
                c = c.substring(1)
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length)
            }
        }
        return ''
    },
    setCookie: function (cname, cvalue, exdays) {
        cvalue = typeof cvalue !== 'string' ? JSON.stringify(cvalue) : cvalue
        var d = new Date()
        d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
        var expires = 'expires=' + d.toUTCString()
        document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'
    },
    removeCookie: function (cname) {
        document.cookie =
            cname + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;'
    },
    checkCookie: function (cname) {
        var cookie = cookieManager.getCookie(cname)
        if (cookie !== '') {
            return true
        }
        return false
    }
}

export default cookieManager
