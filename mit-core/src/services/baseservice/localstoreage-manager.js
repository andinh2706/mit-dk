/* global localStorage */
/* eslint no-undef: "error" */
const TOKEN = 'TOKEN';
const USER_INFO = 'USER_INFO';

// We can use cookie instead
const localStoreageManager = {
    getToken: function () {
        return localStorage.getItem(TOKEN)
    },
    getUserInfo: function () {
        return localStorage.getItem(USER_INFO)
    },
    setToken: function (token) {
        if (typeof token !== 'string') {
            localStorage.setItem(TOKEN, token.toString())
        } else {
            localStorage.setItem(TOKEN, token)
        }
    },
    setUserInfo: function (userInfo) {
        if (typeof userInfo !== 'string') {
            localStorage.setItem(USER_INFO, JSON.stringify(userInfo))
        } else {
            localStorage.setItem(USER_INFO, userInfo)
        }
    },
    hasToken: function () {
        return !!localStoreageManager.getToken()
    },
    removeToken: function () {
        localStorage.removeItem(TOKEN)
    },
    removeUserInfo: function () {
        localStorage.removeItem(USER_INFO)
    }
}

export default localStoreageManager
