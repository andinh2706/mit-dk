const config = {
    env: process.env.REACT_APP_ENV || 'local',
    masterKey: process.env.REACT_APP_MASTERKEY || '',
    local: {
        baseApi: process.env.REACT_APP_BASE_API || 'http://localhost:9000'
    },
    development: {
        baseApi: process.env.REACT_APP_BASE_API || 'http://localhost:9000'
    },
    staging: {
        baseApi: process.env.REACT_APP_BASE_API || 'http://localhost:9000'
    },
    production: {
        baseApi: process.env.REACT_APP_BASE_API || 'http://localhost:9000'
    }
}

export const masterKey = config.masterKey
export default config[config.env]
