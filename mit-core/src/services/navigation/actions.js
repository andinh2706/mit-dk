import { GOTO, BACK, FORWARD } from '../../const';


export const goto = (routerDispatcher, url) => {
    routerDispatcher({ type: GOTO, url })
}

export const back = (routerDispatcher) => {
    routerDispatcher({ type: BACK  });
}

export const forward = (routerDispatcher) => {
    routerDispatcher({ type: FORWARD });
}