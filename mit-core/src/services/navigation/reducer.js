import { GOTO, BACK, FORWARD } from '../../const'

export const initRouter = (router) => {
    return {
        router
    };
};


export const routerReducer = (state, action) => {
    switch (action.type) {
        case GOTO:
            state.router && state.router.push(action.url);
            break;
        case BACK:
            state.router && state.router.goBack();
            break;
        case FORWARD:
            state.router && state.router.goForward();
            break;
    }
    return state;
}
