import React from 'react'

import { ExampleComponent } from 'mit-core'
import 'mit-core/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
