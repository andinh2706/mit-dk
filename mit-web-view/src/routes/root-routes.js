import React from 'react'
import ToDoList from '../scenes/todo/todo-list/todo-list'
import ToDoInput from '../scenes/todo/todo-input/todo-input'
import MainPage from '../scenes/main-page/main-page'
const routes = [
    {
        path: '/hello',
        component: <h1>Hello World !!!</h1>
    },
    { path: '/todo', component: <ToDoList /> },
    { path: '/todo-input', component: <ToDoInput /> },
    { path: '/', component: <MainPage /> }
]

export default routes
