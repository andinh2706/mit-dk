import './App.css'
import React, { useReducer } from 'react'
import {
    NavigationContext,
    routerReducer,
    initRouter,
    ToDoContext,
    initTodo,
    todoReducer,
    ModalContext,
    initModal,
    modalReducer,
    AuthenticateContext,
    initAuth,
    authReducer
} from 'mit-core'
import { useHistory } from 'react-router-dom'
import rootRoutes from './routes/root-routes'
import routable from './hoc/routable'
import mainLayout from './layouts/main-layout/main-layout'
import Header from './components/header/header'
import Footer from './components/footer/footer'
import Modal from './components/modal/modal'

const RoutableBody = routable(rootRoutes)
const MainLayout = mainLayout({
    header: <Header />,
    body: <RoutableBody />,
    footer: <Footer />,
    headerSize: '56px',
    footerSize: '58px'
})

function App() {
    const [router, routerDispatcher] = useReducer(
        routerReducer,
        initRouter(useHistory())
    )
    const [stateTodo, todoDispatcher] = useReducer(todoReducer, initTodo)
    const [modalState, modalDispatcher] = useReducer(modalReducer, initModal)
    const [authState, authDispatcher] = useReducer(authReducer, initAuth)
    return (
        <NavigationContext.Provider value={{ router, routerDispatcher }}>
            <AuthenticateContext.Provider value={{ authState, authDispatcher }}>
                <ToDoContext.Provider value={{ stateTodo, todoDispatcher }}>
                    <ModalContext.Provider
                        value={{ modalState, modalDispatcher }}
                    >
                        <MainLayout />
                        <Modal />
                    </ModalContext.Provider>
                </ToDoContext.Provider>
            </AuthenticateContext.Provider>
        </NavigationContext.Provider>
    )
}

export default App
