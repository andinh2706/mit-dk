import React from 'react'

const mainLayout = ({ header, body, footer, headerSize, footerSize }) => {
    return () => (
        <React.Fragment>
            {header}
            <div style={{ marginTop: headerSize || 0 }}></div>
            {body}
            <div style={{ marginBottom: footerSize || 0 }}></div>
            {footer}
        </React.Fragment>
    )
}

export default mainLayout
