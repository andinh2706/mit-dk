import React, { useContext } from 'react'
import styles from './todo-list.module.css'
import { ToDoContext, removeTodo, goto, NavigationContext } from 'mit-core'
import Button from '../../../components/button/button'
import List from '../../../components/list/list'

const ToDoList = () => {
    const { stateTodo, todoDispatcher } = useContext(ToDoContext)
    const { routerDispatcher } = useContext(NavigationContext)
    return (
        <div className={styles.listContainer}>
            <Button
                onClick={() => goto(routerDispatcher, '/todo-input')}
                style={{ 'margin-bottom': '15px' }}
            >
                Add
            </Button>
            <List
                todos={stateTodo.todos}
                onClickItem={removeTodo.bind(null, todoDispatcher)}
                itemWidth='400px'
            />
        </div>
    )
}

export default ToDoList
