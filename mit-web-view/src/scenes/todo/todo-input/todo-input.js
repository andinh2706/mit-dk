import React, { useRef, useContext } from 'react'
import styles from './todo-input.module.css'
import { ToDoContext, addTodo, goto, NavigationContext } from 'mit-core'
import Button from '../../../components/button/button'
import TextInput from '../../../components/text-input/text-input'

const ToDoInput = () => {
    const inputEl = useRef(null)
    const { todoDispatcher } = useContext(ToDoContext)
    const { routerDispatcher } = useContext(NavigationContext)

    const addTodoHandler = () => {
        const callback = () => {
            goto(routerDispatcher, 'todo')
        }
        addTodo(todoDispatcher, inputEl.current.value, callback)
        inputEl.current.value = ''
    }

    return (
        <div className={styles['inputContainer']}>
            <TextInput
                placeholder='Add Todo'
                forwardRef={inputEl}
                style={{ marginRight: '10px', width: '60%' }}
            />
            <Button onClick={() => addTodoHandler()}>Add</Button>
        </div>
    )
}
export default ToDoInput
