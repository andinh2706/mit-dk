import React, { useContext, useRef } from 'react'
import Button from '../../components/button/button'
import TextInput from '../../components/text-input/text-input'
import { login, AuthenticateContext, ModalContext, closeModal } from 'mit-core'
import { useHistory } from 'react-router-dom'

const Login = () => {
    const { authState, authDispatcher } = useContext(AuthenticateContext)
    const { modalDispatcher } = useContext(ModalContext)
    const history = useHistory()

    const usernameEl = useRef(null)
    const passwordEl = useRef(null)
    const loginHandler = () => {
        const username = usernameEl.current.value
        const password = passwordEl.current.value
        login(authDispatcher, { username, password }).then((success) => {
            if (success) {
                closeModal(modalDispatcher)
                history.push('/')
            }
        })
    }

    const error = authState.error ? <p>Incorrect username or password</p> : null

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'column',
                height: '30vh'
            }}
        >
            <h4>Login</h4>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'column',
                    width: '90%'
                }}
            >
                <TextInput
                    placeholder='User Name'
                    forwardRef={usernameEl}
                    style={{ marginBottom: '7px', width: '100%' }}
                />
                <TextInput
                    placeholder='Password'
                    forwardRef={passwordEl}
                    style={{ marginBottom: '7px', width: '100%' }}
                />
                {error}
            </div>
            <Button onClick={loginHandler}>Login</Button>
        </div>
    )
}

export default Login
