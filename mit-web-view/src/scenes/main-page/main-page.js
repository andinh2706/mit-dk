import React, { useContext } from 'react'
import { AuthenticateContext } from 'mit-core'
const MainPage = () => {
    const { authState } = useContext(AuthenticateContext)
    return !authState.isAuth ? (
        <h1>Main Page!</h1>
    ) : (
        <h1>Welcome {authState.username}</h1>
    )
}

export default MainPage
