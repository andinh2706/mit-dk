import React from 'react'
import styles from './text-input.module.css'

const TextInput = (props) => {
    const { forwardRef } = props
    return (
        <input
            type='text'
            className={styles['text-input']}
            {...props}
            ref={forwardRef}
        />
    )
}

export default TextInput
