import React, { useContext } from 'react'

import styles from './modal.module.css'
import Backdrop from '../backdrop/backdrop'
import { ModalContext, closeModal } from 'mit-core'

const Modal = (props) => {
    const { modalState, modalDispatcher } = useContext(ModalContext)
    return (
        <React.Fragment>
            <Backdrop
                show={modalState.show}
                clicked={() => closeModal(modalDispatcher)}
            />
            <div
                className={styles.modal}
                style={{
                    transform: modalState.show
                        ? 'translateY(0)'
                        : 'translateY(-100vh)',
                    opacity: modalState.show ? '1' : '0'
                }}
            >
                <div
                    style={{
                        position: 'absolute',
                        top: '10px',
                        right: '10px',
                        cursor: 'pointer'
                    }}
                    onClick={() => closeModal(modalDispatcher)}
                >
                    X
                </div>
                {modalState && modalState.content}
            </div>
        </React.Fragment>
    )
}
// class Modal extends Component {
//     shouldComponentUpdate(nextProps, nextState) {
//         return (
//             nextProps.show !== this.props.show ||
//             nextProps.children !== this.props.children
//         )
//     }

//     render() {
//         return (
//             <React.Fragment>
//                 <Backdrop
//                     show={this.props.show}
//                     clicked={this.props.modalClosed}
//                 />
//                 <div
//                     className={styles.modal}
//                     style={{
//                         transform: this.props.show
//                             ? 'translateY(0)'
//                             : 'translateY(-100vh)',
//                         opacity: this.props.show ? '1' : '0'
//                     }}
//                 >
//                     <div
//                         style={{
//                             position: 'absolute',
//                             top: '10px',
//                             right: '10px',
//                             cursor: 'pointer'
//                         }}
//                     >
//                         X
//                     </div>
//                     {this.props.children}
//                 </div>
//             </React.Fragment>
//         )
//     }
// }

export default Modal
