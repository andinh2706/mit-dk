import React, { useState } from 'react'

import Toolbar from '../navigation/toolbar/toolbar'
import SideDrawer from '../navigation/side-drawer/side-drawer'

const Header = (props) => {
    const [showSideDrawer, setShowSideDrawer] = useState(false)
    const sideDrawerToggleHandler = () => {
        setShowSideDrawer((currentState) => !currentState)
    }
    const sideDrawerClosedHandler = () => {
        setShowSideDrawer(false)
    }

    return (
        <React.Fragment>
            <Toolbar drawerToggleClicked={sideDrawerToggleHandler} />
            <SideDrawer
                open={showSideDrawer}
                closed={sideDrawerClosedHandler}
            />
        </React.Fragment>
    )
}
export default Header
