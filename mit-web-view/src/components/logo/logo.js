import React from 'react'

import mitLogo from '../../assets/images/mit-dk-logo.png'
import styles from './logo.module.css'

const Logo = (props) => (
    <div className={styles.logo} style={{ height: props.height }}>
        <img src={mitLogo} alt='Mit.dk' />
    </div>
)

export default Logo
