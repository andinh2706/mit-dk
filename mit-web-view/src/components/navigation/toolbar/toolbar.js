import React from 'react'

import styles from './toolbar.module.css'
import Logo from '../../logo/logo'
import NavigationItems from '../navigation-items/navigation-items'
import DrawerToggle from '../side-drawer/drawer-toggle/drawer-toggle'

const Toolbar = (props) => (
    <header className={styles.toolbar}>
        <DrawerToggle clicked={props.drawerToggleClicked} />
        <div className={styles.logo}>
            <Logo />
        </div>
        <nav className={styles['desktop-only']}>
            <NavigationItems />
        </nav>
    </header>
)

export default Toolbar
