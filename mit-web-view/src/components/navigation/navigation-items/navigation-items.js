import React, { useState, useContext } from 'react'

import styles from './navigation-items.module.css'
import NavigationItem from './navigation-item/navigation-item'
import { useHistory } from 'react-router'
import { AuthenticateContext } from 'mit-core'

const NavigationItems = (props) => {
    const { authState } = useContext(AuthenticateContext)
    const history = useHistory()
    const [currentLocation, setCurrentLocation] = useState(
        history.location.pathname
    )

    return (
        <ul className={styles['navigation-list']}>
            <NavigationItem
                link='/'
                active={currentLocation === '/'}
                onClick={() => setCurrentLocation('/')}
            >
                Home
            </NavigationItem>
            <NavigationItem
                link='/hello'
                active={currentLocation === '/hello'}
                onClick={() => setCurrentLocation('/hello')}
            >
                Hello world
            </NavigationItem>
            {authState.isAuth ? (
                <React.Fragment>
                    <NavigationItem
                        link='/todo'
                        active={currentLocation === '/todo'}
                        onClick={() => setCurrentLocation('/todo')}
                    >
                        Todo List
                    </NavigationItem>
                    <NavigationItem link='/login'>Logout</NavigationItem>
                </React.Fragment>
            ) : (
                <NavigationItem link='/login'>Login</NavigationItem>
            )}
        </ul>
    )
}

export default NavigationItems
