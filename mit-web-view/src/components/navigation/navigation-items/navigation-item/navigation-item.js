import React, { useContext } from 'react'
import { useHistory } from 'react-router'
// import { NavLink } from 'react-router-dom'
import { ModalContext, openModal } from 'mit-core'
import styles from './navigation-item.module.css'
import Login from '../../../../scenes/login/login'

const NavigationItem = (props) => {
    const history = useHistory()
    const { modalDispatcher } = useContext(ModalContext)
    const clickHandler = () => {
        if (props.link === '/login') {
            openModal(modalDispatcher, <Login />)
        } else {
            props.onClick()
            history.push(props.link)
        }
    }
    return (
        <li className={styles['navigation-item']}>
            <div
                onClick={clickHandler}
                className={props.active ? styles['active'] : ''}
            >
                {props.children}
            </div>
        </li>
    )
}

export default NavigationItem
