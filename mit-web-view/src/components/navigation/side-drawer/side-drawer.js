import React from 'react'

import Logo from '../../logo/logo'
import Backdrop from '../../backdrop/backdrop'
import NavigationItems from '../navigation-items/navigation-items'
import styles from './side-drawer.module.css'

const SideDrawer = (props) => {
    let attachedClasses = [styles['side-drawer'], styles.close]
    if (props.open) {
        attachedClasses = [styles['side-drawer'], styles.open]
    }
    return (
        <React.Fragment>
            <Backdrop show={props.open} clicked={props.closed} />
            <div className={attachedClasses.join(' ')} onClick={props.closed}>
                <div className={styles.logo}>
                    <Logo />
                </div>
                <nav>
                    <NavigationItems />
                </nav>
            </div>
        </React.Fragment>
    )
}

export default SideDrawer
