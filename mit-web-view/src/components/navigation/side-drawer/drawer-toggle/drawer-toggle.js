import React from 'react'

import styles from './drawer-toggle.module.css'

const DrawerToggle = (props) => (
    <div className={styles['drawer-toggle']} onClick={props.clicked}>
        <div></div>
        <div></div>
        <div></div>
    </div>
)

export default DrawerToggle
