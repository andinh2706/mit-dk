import React from 'react'
import ListItem from './list-item/list-item'

const List = (props) => {
    const { todos, onClickItem, itemWidth } = props
    return (
        <ul>
            {todos.map((todo) => (
                <ListItem
                    key={todo}
                    onClick={() => onClickItem(todo)}
                    style={{
                        width: itemWidth
                    }}
                >
                    {todo}
                </ListItem>
            ))}
        </ul>
    )
}

export default List
