import React from 'react'
import styles from './list-item.module.css'

const ListItem = (props) => {
    return (
        <li className={styles['list-item']} {...props}>
            {props.children}
        </li>
    )
}

export default ListItem
