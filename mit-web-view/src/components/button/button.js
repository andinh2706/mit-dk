import React from 'react'
import styles from './button.module.css'

const Button = (props) => {
    const { btnType } = props
    const className = `${styles['btn']} ${
        btnType && btnType === 'danger'
            ? styles['danger']
            : btnType === 'warning'
            ? styles['warning']
            : ''
    }`

    return (
        <button
            onClick={props.onClick}
            className={className}
            style={props.styles}
        >
            {props.children}
        </button>
    )
}

export default Button
