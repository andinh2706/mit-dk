import React from 'react'
import styles from './footer.module.css'

const Footer = () => {
    return (
        <div className={styles['app-footer']}>
            <p>Mit.dk - experimental version</p>
        </div>
    )
}

export default Footer
