import React from 'react'
import { Switch, Route } from 'react-router-dom'

const routable = (routes) => {
    return () => (
        <Switch>
            {routes.map((route) => {
                return (
                    <Route path={route.path} key={route.path}>
                        {route.component}
                    </Route>
                )
            })}
        </Switch>
    )
}

export default routable
